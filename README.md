
# Rancher-API

[![pipeline status](https://gitlab.com/alphayax/rancher-api/badges/master/pipeline.svg)](https://gitlab.com/alphayax/rancher-api/commits/master)
[![coverage report](https://gitlab.com/alphayax/rancher-api/badges/master/coverage.svg)](https://gitlab.com/alphayax/rancher-api/commits/master)

A very basic tool based on the RancherAPI

## Install

This package is available in composer. To use it, just require as follow :

```bash
composer require alphayax/rancher-api
```


## How to use

> Note that you need to generate a API Key in rancher to use this. (Under API -> Keys menu)

### Update a Rancher service with a new docker image

> Useful for Continuous Deployment

```php
$client = new \alphayax\rancher_api\Client($rancherUrl, $rancherKey, $rancherSecret);
$client->upgradeFromProjectIdAndServiceId($projectId, $serviceId);
```

### CLI

You can use the CLI script : `vendor/bin/rancher_update.php`

### Continuous Deployment

Basic gitlab CI example

> The values of`$RANCHER_SECRET`, `$RANCHER_KEY` and `$RANCHER_URL` are available under the API -> Key menu in rancher 
 
- In gitlab repository settings, under CI/CD configuration, add the following secret variables :
  - `$RANCHER_URL` (eg: `http://www.example.com:8080/v2-beta`)  
  - `$RANCHER_SECRET` (eg: `sdf45gsdfg12sd3f1gs5dfg1sd23fg1s5df1`)        
  - `$RANCHER_KEY` (eg: `123456789ABCDEF12345`)
  - `$RANCHER_ENVIRONMENT` (eg: `1a2`)

- In your `.gitlab-ci.yaml` file, declare the `deploy` stage and put the following inside :

```yaml
.deploy: &deploy
  stage: deploy
  image: composer
  variables:
    GIT_STRATEGY: none
    RANCHER_SERVICE: 1s220
  tags:
  - composer
  only:
  - master
  script:
  - composer require alphayax/rancher-api
  - vendor/bin/rancher_update.php --rancher-url $RANCHER_URL --rancher-secret $RANCHER_SECRET --rancher-key $RANCHER_KEY --rancher-project-id $RANCHER_ENVIRONMENT --rancher-service-id $RANCHER_SERVICE
```
