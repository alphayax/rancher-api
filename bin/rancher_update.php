#!/usr/bin/env php
<?php

foreach (['/../', '/../../', '/../../../', '/../vendor/', '/vendor/'] as $file) {
    $autoloader_afi = __DIR__ . $file . 'autoload.php';
    if (file_exists($autoloader_afi)) {
        require_once $autoloader_afi;
        break;
    }
}
unset($file);
unset($autoloader_afi);


$Opt = \alphayax\utils\cli\GetOpt::getInstance();
$Opt->setDescription('Update a service on rancher');
$Opt->addLongOpt('rancher-url', 'URL of the rancher server', true);
$Opt->addLongOpt('rancher-key', 'API Key', true);
$Opt->addLongOpt('rancher-secret', 'API Secret', true);
$Opt->addLongOpt('rancher-project-id', 'Project Id', true);
$Opt->addLongOpt('rancher-service-id', 'Service Id', true);
$Opt->parse();

/// Initialize the client
$rancherUrl = $Opt->getValueName('rancher-url') ?: @getenv('RANCHER_URL');
$rancherKey = $Opt->getValueName('rancher-key') ?: @getenv('RANCHER_KEY');
$rancherSecret = $Opt->getValueName('rancher-secret') ?: @getenv('RANCHER_SECRET');
$client = new \alphayax\rancher_api\Client($rancherUrl, $rancherKey, $rancherSecret);

/// Upgrade project
$projectId = $Opt->getValueName('rancher-project-id') ?: @getenv('RANCHER_URL');
$serviceId = $Opt->getValueName('rancher-service-id') ?: @getenv('RANCHER_URL');
$client->upgradeFromProjectIdAndServiceId($projectId, $serviceId);

