<?php

namespace alphayax\rancher_api\exceptions;

/**
 * Class Exception
 * Base exception class
 * @package alphayax\rancher_api\exceptions
 */
abstract class Exception extends \Exception
{

}
