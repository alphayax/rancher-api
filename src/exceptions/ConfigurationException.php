<?php

namespace alphayax\rancher_api\exceptions;

/**
 * Class ConfigurationException
 * @package alphayax\rancher_api\exceptions
 */
class ConfigurationException extends Exception
{

}
