<?php

namespace alphayax\rancher_api\resources;

/**
 * Class Service
 * @package alphayax\rancher_api\resources
 */
class Service extends AbstractResource
{

    /** @var string */
    protected $state;

    protected $upgrade;


    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }


    /**
     * @throws \Exception
     */
    public function upgrade()
    {
        $client = new \GuzzleHttp\Client();

        $res = $client->post($this->getActionUrl('upgrade'), [
            'auth' => $this->client->getAuthentication(),
            'json' => [
                'inServiceStrategy' => $this->upgrade->inServiceStrategy,
                'toServiceStrategy' => $this->upgrade->inServiceStrategy,
            ],
        ]);

        if ($res->getStatusCode() !== 202) {
            throw new \Exception('Want 202 : Get ' . $res->getStatusCode());
        }

        $service_o = json_decode($res->getBody());
        $this->init( $service_o);
    }

    /**
     * @throws \Exception
     */
    public function update()
    {
        $client = new \GuzzleHttp\Client();

        /*
        $res = $client->post($this->getActionUrl('update'), [
            'auth' => $this->client->getAuthentication(),
        ]);
        */
        $res = $client->get($this->getLinkUrl('self'), [
            'auth' => $this->client->getAuthentication(),
        ]);

        $service_o = json_decode($res->getBody());
        $this->init( $service_o);
    }

    /**
     * @throws \Exception
     */
    public function finishUpgrade()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->post($this->getActionUrl('finishupgrade'), [
            'auth' => $this->client->getAuthentication(),
        ]);

        if ($res->getStatusCode() !== 202) {
            throw new \Exception('Want 202 : Get ' . $res->getStatusCode());
        }

        $service_o = json_decode($res->getBody());
        $this->init( $service_o);
    }

}
