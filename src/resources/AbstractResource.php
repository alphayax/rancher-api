<?php

namespace alphayax\rancher_api\resources;

use alphayax\rancher_api\Client;

/**
 * Class AbstractResource
 * @package alphayax\rancher_api\resources
 */
abstract class AbstractResource
{

    /** @var Client */
    protected $client;

    /** @var string[] */
    protected $links = [];

    /** @var string[] */
    protected $actions = [];

    /**
     * Service constructor.
     * @param \alphayax\rancher_api\Client $client
     * @param \stdClass                    $service_o
     */
    public function __construct(Client $client, \stdClass $service_o)
    {
        $this->client = $client;
        $this->init($service_o);
    }


    /**
     * @param \stdClass $service_o
     */
    protected function init(\stdClass $service_o)
    {
        $properties = get_object_vars( $service_o);
        foreach ($properties as $propertyName => $propertyValue){
            if( property_exists( static::class, $propertyName)){
                $this->$propertyName = $propertyValue;
            }
        }
    }

    /**
     * @param $actionName
     * @return string
     * @throws \Exception
     */
    protected function getActionUrl($actionName)
    {
        $actionUrl = @$this->actions->$actionName;
        if (empty($actionUrl)) {
            throw new \Exception("Action $actionName is not available right now. Available actions : ".
                implode(', ', array_keys(get_object_vars($this->actions))));
        }
        return $actionUrl;
    }

    /**
     * @param $LinkName
     * @return string
     * @throws \Exception
     */
    protected function getLinkUrl($LinkName)
    {
        $linkUrl = @$this->links->$LinkName;
        if (empty($linkUrl)) {
            throw new \Exception("Link $LinkName is not available right now. Available links : ".
                implode(', ', array_keys(get_object_vars($this->links))));
        }
        return $linkUrl;
    }

}
