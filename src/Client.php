<?php

namespace alphayax\rancher_api;

use alphayax\rancher_api\exceptions\ConfigurationException;
use alphayax\rancher_api\resources\Service;

/**
 * Class Client
 * @package alphayax\rancher_api
 */
class Client
{
    /** @var string */
    protected $rancherUrl;

    /** @var string */
    protected $rancherKey;

    /** @var string */
    protected $rancherSecret;

    /**
     * Client constructor.
     * @param string $url
     * @param string $key
     * @param string $secret
     * @throws \alphayax\rancher_api\exceptions\ConfigurationException
     */
    public function __construct(string $url, string $key, string $secret)
    {
        if( empty( $url)){
            throw new ConfigurationException('Rancher URL is not set');
        }

        if( empty( $key)){
            throw new ConfigurationException('Rancher API Key is not set');
        }

        if( empty( $secret)){
            throw new ConfigurationException('Rancher API Secret not set');
        }

        $this->rancherUrl = $url;
        $this->rancherSecret = $secret;
        $this->rancherKey = $key;
    }

    /**
     * @return array
     */
    public function getAuthentication()
    {
        return [
            $this->rancherKey,
            $this->rancherSecret,
        ];
    }

    /**
     * @param $projectId
     * @param $serviceId
     * @return \alphayax\rancher_api\resources\Service
     * @throws \Exception
     */
    public function getFromProjectIdAndServiceId($projectId, $serviceId) : Service
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get("{$this->rancherUrl}/projects/$projectId/services/$serviceId", [
            'auth' => $this->getAuthentication(),
        ]);

        if ($res->getStatusCode() !== 200) {
            throw new \Exception('Want 200 : Get ' . $res->getStatusCode());
        }

        $service_o = json_decode($res->getBody(), false);

        return new Service($this, $service_o);
    }

    /**
     * @param $projectId
     * @param $serviceId
     * @throws \Exception
     */
    public function upgradeFromProjectIdAndServiceId($projectId, $serviceId)
    {
        $service = $this->getFromProjectIdAndServiceId( $projectId, $serviceId);

        echo "Starting Upgrade" . PHP_EOL;
        $service->upgrade();

        while ($service->getState() != 'upgraded') {
            echo '.';
            $service->update();
            sleep(1);
        }
        echo PHP_EOL;

        echo "Finishing Upgrade". PHP_EOL;
        $service->finishUpgrade();

        echo "Upgrade done :)" . PHP_EOL . PHP_EOL;
    }

}
